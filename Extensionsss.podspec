Pod::Spec.new do |s|

  s.name            = "Extensionsss"
  s.version         = "1.1.0"
  s.summary         = "Extensions helpers for Yesss Apps."
  s.description     = "Extensions helpers to be used in Yesss Applications."
  s.homepage        = "https://djasec@bitbucket.org/a4yb/extensions"
  s.license         = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author          = { "Djamil Secco" => "ds@yesss.lu" }
  s.platform        = :ios, "12.0"
  s.source          = { :git => "https://djasec@bitbucket.org/a4yb/extensions.git", :tag => "#{s.version}" }
  s.source_files    = "Extensionsss", "Extensionsss/Extensions"
  s.swift_version   = "4.2"

end
