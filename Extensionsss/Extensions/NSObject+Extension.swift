//
//  NSObject+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension NSObject {
  public class var nameOfClass : String {
    return NSStringFromClass(self).components(separatedBy: ".").last!
  }
  
  public var nameOfClass : String {
    return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
  }
}

