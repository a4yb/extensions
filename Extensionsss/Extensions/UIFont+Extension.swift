//
//  UIFont+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UIFont {
  var bold: UIFont {
    return with(traits: .traitBold)
  }
  
  var italic: UIFont {
    return with(traits: .traitItalic)
  }
  
  var boldItalic: UIFont {
    return with(traits: [.traitBold, .traitItalic])
  }
  
  func with(traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
    guard let descriptor = self.fontDescriptor.withSymbolicTraits(traits) else {
      return self
    }
    return UIFont(descriptor: descriptor, size: 0)
  }
}
