//
//  Bool+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension Bool {
  func string01() -> String {
    return self ? "1" : "0"
  }
  
  func stringYN() -> String {
    return self ? "Y" : "N"
  }
}
