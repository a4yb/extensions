//
//  Optional+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension Optional {
  
  public func isNull(someObject: Any?) -> Bool {
    guard let someObject = someObject else {
      return true
    }
    guard "\(someObject)" != "<null>" else {
      return true
    }
    return (someObject is NSNull)
  }
  
  func toBool() -> Bool {
    guard let value = self else { return false }
    return ["1", "y", "true"].contains("\(value)".lowercased())
  }
  
  func toString() -> String {
    guard let value = self else { return "" }
    return "\(value)"
  }
  
  func toInt() -> Int {
    guard let value = self else { return 0 }
    return "\(value)".toInt()
  }
  
  func toUInt() -> UInt {
    guard let value = self else { return 0 }
    return "\(value)".toUInt()
  }
  
  func toDouble() -> Double {
    guard let value = self else { return 0 }
    return "\(value)".toDouble()
  }
  
  func toFloat() -> Float {
    guard let value = self else { return 0 }
    return "\(value)".toFloat()
  }
  
  func toDictionary() -> [String:Any] {
    return self as? [String:Any] ?? [String:Any]()
  }
  
}
