//
//  Double+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension Double {
  func rounded(decimalPlaces: Int) -> Double {
    guard decimalPlaces > -1 else { return self }
    let factor = pow(10.0, Double(decimalPlaces))
    let valueToRound = self * factor
    let roundedValue = valueToRound.rounded(.up)
    return roundedValue / factor
  }
  
  func string(fractionDigits:Int) -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.minimumFractionDigits = fractionDigits
    formatter.maximumFractionDigits = fractionDigits
    return formatter.string(from: self as NSNumber) ?? "\(self)"
  }
  
  func percentage(fractionDigits:Int) -> String {
    let formatter = NumberFormatter()
    formatter.minimumFractionDigits = fractionDigits
    formatter.maximumFractionDigits = fractionDigits
    let returnString = formatter.string(from: self as NSNumber) ?? "\(self)"
    return returnString  + " %"
  }
  
  func currencyFormat(fractionDigits:Int) -> String {
    let formatter = NumberFormatter()
    formatter.locale = Locale.current
    formatter.numberStyle = .currency
    formatter.minimumFractionDigits = fractionDigits
    formatter.maximumFractionDigits = fractionDigits
    return formatter.string(from: self as NSNumber) ?? "\(self)"
  }
  
  func currencyAccounting(fractionDigits:Int) -> String {
    let formatter = NumberFormatter()
    formatter.locale = Locale.current
    formatter.numberStyle = .currencyAccounting
    formatter.minimumFractionDigits = fractionDigits
    formatter.maximumFractionDigits = fractionDigits
    return formatter.string(from: self as NSNumber) ?? "\(self)"
  }
  
  func currencyISOCode(fractionDigits:Int) -> String {
    let formatter = NumberFormatter()
    formatter.locale = Locale.current
    formatter.numberStyle = .currencyISOCode
    formatter.minimumFractionDigits = fractionDigits
    formatter.maximumFractionDigits = fractionDigits
    return formatter.string(from: self as NSNumber) ?? "\(self)"
  }
  
  func scaleFormat() -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    var unit = ""
    var value = self.string(fractionDigits: 1)
    switch self {
    case 0..<1000:
      unit = ""
      value = self.string(fractionDigits: 1)
    case 1000..<1000000:
      unit = "k"
      value = (self/1000).string(fractionDigits: 1)
    case 1000000..<1000000000:
      unit = "M"
      value = (self/1000000).string(fractionDigits: 1)
    case 1000000000..<1000000000000:
      unit = "G"
      value = (self/1000000000).string(fractionDigits: 1)
    case 1000000000000..<1000000000000000:
      unit = "T"
      value = (self/1000000000000).string(fractionDigits: 1)
    case 1000000000000000..<1000000000000000000:
      unit = "P"
      value = (self/1000000000000000).string(fractionDigits: 1)
    case 1000000000000000000..<1000000000000000000000:
      unit = "E"
      value = (self/1000000000000000000).string(fractionDigits: 1)
    case 1000000000000000000000..<1000000000000000000000000:
      unit = "Z"
      value = (self/1000000000000000000000).string(fractionDigits: 1)
    case 1000000000000000000000000..<1000000000000000000000000000:
      unit = "Y"
      value = (self/1000000000000000000000000).string(fractionDigits: 1)
    default:
      unit = ""
    }
    return "\(value)\(unit)"
  }
}
