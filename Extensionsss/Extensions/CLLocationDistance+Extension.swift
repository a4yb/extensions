//
//  CLLocationDistance+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import MapKit

public extension CLLocationDistance {
  
  @available(iOS 10.0, *)
  func formattedForCountry() -> String {
    let formatter = MeasurementFormatter()
    formatter.locale = Locale.current
    formatter.numberFormatter.maximumFractionDigits = 2
    let distance  = Measurement(value: self, unit: UnitLength.meters) as Measurement
    return formatter.string(from: distance)
  }
  
}

public extension CLLocationCoordinate2D {
  /*
   func closestLocation(locations:[CLLocationCoordinate2D]) -> CLLocationCoordinate2D? {
   // find closest location to self and return it
   }
   
   func locationsSortedByDistanceFromPreviousLocation(locations: [CLLocationCoordinate2D]) -> [CLLocationCoordinate2D] {
   // take in an array and a starting location
   // using the previous closestLocation function, build a distance-sorted array
   // return that array
   }
   
   func calculateDirections(toLocation: CLLocationCoordinate2D, transportType: MKDirectionsTransportType, completionHandler: MKDirectionsHandler) {
   // this is really just a convenience wrapper for doing MapKit calculation of directions
   }
   
   func calculateDirections(locations: [CLLocationCoordinate2D], transportType: MKDirectionsTransportType, completionHandler: ([MKRoute]) -> Void) {
   // one by one, iterate through the locations in the array
   // calculate their directions and add the route to an array
   // once you've got all the routes in an array and made it through the whole array, call the completion handler passing in the array of *all* the MKRoutes for every point
   }
   
   Using these methods will look something like this in our view controller:
   
   let sortedDirections = startLocation. locationsSortedByDistanceFromPreviousLocation(yourLocationArray)
   
   CLLocationCoordinate2D.calculateDirection(sortedDirections, transportType: .Automobile) { routes in
   // call method which adds the array of routes to the map
   }
   
   source: http://codereview.stackexchange.com/questions/98753/recursive-function-to-show-directions-for-multiple-locations
   */
}
