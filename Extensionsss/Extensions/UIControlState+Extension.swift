//
//  UIControlState+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UIControl.State {
  public static var Normal: UIControl.State { return [] }
}
