//
//  UIApplication+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UIApplication {
  func applicationName() -> String {
    return Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
  }
  
  func applicationVersion() -> String {
    return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
  }
  
  func applicationBuild() -> String {
    return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
  }
  
  func versionBuild() -> String {
    let version = self.applicationVersion()
    let build = self.applicationBuild()
    return "v\(version)(\(build))"
  }
}
