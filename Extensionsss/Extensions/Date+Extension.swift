//
//  Date+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

let componentFlags : Set<Calendar.Component> = [Calendar.Component.year, Calendar.Component.month, Calendar.Component.day, Calendar.Component.weekdayOrdinal, Calendar.Component.hour,Calendar.Component.minute, Calendar.Component.second, Calendar.Component.weekday, Calendar.Component.weekdayOrdinal]


public extension Date {
  //Crea una data direttamente dai valori passati
  static func customDate(year ye:Int, month mo:Int, day da:Int, hour ho:Int, minute mi:Int, second se:Int) -> Date {
    var comps = DateComponents()
    comps.year = ye
    comps.month = mo
    comps.day = da
    comps.hour = ho
    comps.minute = mi
    comps.second = se
    let date = Calendar.current.date(from: comps)
    return date!
  }
  
  static func customDateUInt(year ye:UInt, month mo:UInt, day da:UInt, hour ho:UInt, minute mi:UInt, second se:UInt) -> Date {
    var comps = DateComponents()
    comps.year = Int(ye)
    comps.month = Int(mo)
    comps.day = Int(da)
    comps.hour = Int(ho)
    comps.minute = Int(mi)
    comps.second = Int(se)
    let date = Calendar.current.date(from: comps)
    return date!
  }
  
  static func dateOfMonthAgo() -> Date {
    return Date().addingTimeInterval(-24 * 30 * 60 * 60)
  }
  
  static func dateOfWeekAgo() -> Date {
    return Date().addingTimeInterval(-24 * 7 * 60 * 60)
  }
  
  func sameDate(ofDate:Date) -> Bool {
    let cal = Calendar.current
    let dif = cal.compare(self, to: ofDate, toGranularity: Calendar.Component.day)
    if dif == .orderedSame {
      return true
    } else {
      return false
    }
  }
  
  static func currentCalendar() -> Calendar {
    return Calendar.autoupdatingCurrent
  }
  
  func isEqualToDateIgnoringTime(_ aDate:Date) -> Bool {
    let components1 = Date.currentCalendar().dateComponents(componentFlags, from: self)
    let components2 = Date.currentCalendar().dateComponents(componentFlags, from: aDate)
    
    return ((components1.year == components2.year) &&
      (components1.month == components2.month) &&
      (components1.day == components2.day))
  }
  
  public func plusSeconds(_ s: UInt) -> Date {
    return self.addComponentsToDate(seconds: Int(s), minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: 0)
  }
  
  public func minusSeconds(_ s: UInt) -> Date {
    return self.addComponentsToDate(seconds: -Int(s), minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: 0)
  }
  
  public func plusMinutes(_ m: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: Int(m), hours: 0, days: 0, weeks: 0, months: 0, years: 0)
  }
  
  public func minusMinutes(_ m: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: -Int(m), hours: 0, days: 0, weeks: 0, months: 0, years: 0)
  }
  
  public func plusHours(_ h: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: Int(h), days: 0, weeks: 0, months: 0, years: 0)
  }
  
  public func minusHours(_ h: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: -Int(h), days: 0, weeks: 0, months: 0, years: 0)
  }
  
  public func plusDays(_ d: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: Int(d), weeks: 0, months: 0, years: 0)
  }
  
  public func minusDays(_ d: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: -Int(d), weeks: 0, months: 0, years: 0)
  }
  
  public func plusWeeks(_ w: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: Int(w), months: 0, years: 0)
  }
  
  public func minusWeeks(_ w: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: -Int(w), months: 0, years: 0)
  }
  
  public func plusMonths(_ m: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: Int(m), years: 0)
  }
  
  public func minusMonths(_ m: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: -Int(m), years: 0)
  }
  
  public func plusYears(_ y: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: Int(y))
  }
  
  public func minusYears(_ y: UInt) -> Date {
    return self.addComponentsToDate(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: -Int(y))
  }
  
  private func addComponentsToDate(seconds sec: Int, minutes min: Int, hours hrs: Int, days d: Int, weeks wks: Int, months mts: Int, years yrs: Int) -> Date {
    var dc:DateComponents = DateComponents()
    dc.second = sec
    dc.minute = min
    dc.hour = hrs
    dc.day = d
    dc.weekOfYear = wks
    dc.month = mts
    dc.year = yrs
    return Calendar.current.date(byAdding: dc, to: self, wrappingComponents: false)!
  }
  
  public func midnightUTCDate() -> Date {
    var dc:DateComponents = Calendar.current.dateComponents([Calendar.Component.year, Calendar.Component.month, Calendar.Component.day], from: self)
    dc.hour = 0
    dc.minute = 0
    dc.second = 0
    dc.nanosecond = 0
    (dc as NSDateComponents).timeZone = TimeZone(secondsFromGMT: 0)
    
    return Calendar.current.date(from: dc)!
  }
  
  public static func secondsBetween(date1 d1:Date, date2 d2:Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.second!
  }
  
  public static func minutesBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.minute!
  }
  
  public static func hoursBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.hour!
  }
  
  public static func daysBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.day!
  }
  
  public static func weeksBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.weekOfYear!
  }
  
  public static func monthsBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.month!
  }
  
  public static func yearsBetween(date1 d1: Date, date2 d2: Date) -> Int {
    let dc = Calendar.current.dateComponents(componentFlags, from: d1, to: d2)
    return dc.year!
  }
  
  //MARK- Comparison Methods
  public func isGreaterThan(_ date: Date) -> Bool {
    return (self.compare(date) == .orderedDescending)
  }
  
  public func isLessThan(_ date: Date) -> Bool {
    return (self.compare(date) == .orderedAscending)
  }
  
  //MARK- Computed Properties
  public var day: UInt {
    return UInt(Calendar.current.component(.day, from: self))
  }
  
  public var weekday: UInt {
    return UInt(Calendar.current.component(.weekday, from: self))
  }
  
  public var month: UInt {
    return UInt(Calendar.current.component(.month, from: self))
  }
  
  public var year: UInt {
    return UInt(Calendar.current.component(.year, from: self))
  }
  
  public var hour: UInt {
    return UInt(Calendar.current.component(.hour, from: self))
  }
  
  public var minute: UInt {
    return UInt(Calendar.current.component(.minute, from: self))
  }
  
  public var second: UInt {
    return UInt(Calendar.current.component(.second, from: self))
  }
  
  public func iso8601() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
    return formatter.string(from: self)
  }
  
  //MARK - Date -> String
  public func stringWithFormat(_ format: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current //Locale.init(identifier: "en-GB")
    dateFormatter.dateFormat = format
    return dateFormatter.string(from: self)
  }
  
  public var dayName: String {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current //Locale.init(identifier: "en-GB")
    dateFormatter.dateFormat = "EEEE"
    return dateFormatter.string(from: self)
  }
  
  public var dayNameShort: String {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current //Locale.init(identifier: "en-GB")
    dateFormatter.dateFormat = "EE"
    return dateFormatter.string(from: self)
  }
  
  public var monthName: String {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current //Locale.init(identifier: "en-GB")
    dateFormatter.dateFormat = "LLLL"
    return dateFormatter.string(from: self)
  }
  
  public var monthNameShort: String {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current //Locale.init(identifier: "en-GB")
    dateFormatter.dateFormat = "LLL"
    return dateFormatter.string(from: self)
  }
}

public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
  return lhs === rhs || lhs.compare(rhs as Date) == .orderedSame
}

public func <(lhs: NSDate, rhs: NSDate) -> Bool {
  return lhs.compare(rhs as Date) == .orderedAscending
}

public func >(lhs: NSDate, rhs: NSDate) -> Bool {
  return lhs.compare(rhs as Date) == .orderedDescending
}

public func delay(_ delay:Double, closure: @escaping ()->()) {
  
  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
    closure()
  }
}
