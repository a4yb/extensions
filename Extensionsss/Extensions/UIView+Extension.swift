//
//  UIView+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
  // Apply clipping mask on certain corners with corner radius
  func layoutCornerRadiusMask(corners: UIRectCorner, cornerRadius: CGFloat) {
    let cornerRadii = CGSize(width: cornerRadius, height: cornerRadius)
    let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: cornerRadii)
    
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    
    layer.mask = mask
  }
  
  // Apply corner radius and rounded shadow path
  func layoutCornerRadiusAndShadow(cornerRadius: CGFloat) {
    // Apply corner radius for background fill only
    layer.cornerRadius = cornerRadius
    
    // Apply shadow with rounded path
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.15
    layer.shadowRadius = 2.0
    layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
  }
  
  func addParallax(X horizontal:Float, Y vertical:Float) {
    let parallaxOnX = UIInterpolatingMotionEffect(keyPath: "center.x", type: UIInterpolatingMotionEffect.EffectType.tiltAlongHorizontalAxis)
    parallaxOnX.minimumRelativeValue = -horizontal
    parallaxOnX.maximumRelativeValue = horizontal
    let parallaxOnY = UIInterpolatingMotionEffect(keyPath: "center.y", type: UIInterpolatingMotionEffect.EffectType.tiltAlongVerticalAxis)
    parallaxOnY.minimumRelativeValue = -vertical
    parallaxOnY.maximumRelativeValue = vertical
    let group = UIMotionEffectGroup()
    group.motionEffects = [parallaxOnX, parallaxOnY]
    self.addMotionEffect(group)
  }
  
  func blurMyBackgroundDark(adjust b:Bool, white v:CGFloat, alpha a:CGFloat) {
    for v in self.subviews {
      if v is UIVisualEffectView {
        v.removeFromSuperview()
      }
    }
    let blur = UIBlurEffect(style: UIBlurEffect.Style.dark)
    let fxView = UIVisualEffectView(effect: blur)
    if b {
      fxView.contentView.backgroundColor = UIColor(white:v, alpha:a)
    }
    fxView.frame = self.bounds
    self.addSubview(fxView)
    self.sendSubviewToBack(fxView)
  }
  
  func blurMyBackgroundLight() {
    for v in self.subviews {
      if v is UIVisualEffectView {
        v.removeFromSuperview()
      }
    }
    let blur = UIBlurEffect(style: UIBlurEffect.Style.light)
    let fxView = UIVisualEffectView(effect: blur)
    var rect = self.bounds
    rect.size.width = CGFloat(2500)
    fxView.frame = rect
    self.addSubview(fxView)
    self.sendSubviewToBack(fxView)
  }
  
  func capture() -> UIImage {
    UIGraphicsBeginImageContextWithOptions(self.frame.size, self.isOpaque, UIScreen.main.scale)
    self.drawHierarchy(in: self.frame, afterScreenUpdates: false)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
  }
  
  func convertRectCorrectly(_ rect: CGRect, toView view: UIView) -> CGRect {
    if UIScreen.main.scale == 1 {
      return self.convert(rect, to: view)
    } else if self == view {
      return rect
    } else {
      var rectInParent = self.convert(rect, to: self.superview)
      rectInParent.origin.x /= UIScreen.main.scale
      rectInParent.origin.y /= UIScreen.main.scale
      let superViewRect = self.superview!.convertRectCorrectly(self.superview!.frame, toView: view)
      rectInParent.origin.x += superViewRect.origin.x
      rectInParent.origin.y += superViewRect.origin.y
      return rectInParent
    }
  }
  
  @IBInspectable
  var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
    }
  }
  
  @IBInspectable
  var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable
  var borderColor: UIColor? {
    get {
      guard let color = layer.borderColor else { return nil }
      return UIColor(cgColor: color)
    }
    set {
      layer.borderColor = newValue?.cgColor
    }
  }
}
