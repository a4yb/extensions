//
//  String+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension String {
  
  //  var capitalizedFirstLetter:String {
  //    let string = self
  //    return string.replacingCharacters(in: startIndex...startIndex, with: String(self[startIndex]).capitalized)
  //  }
  
  func toBool() -> Bool {
    return ["1", "y", "true"].contains(self.lowercased())
  }
  
  func captalizeFirstCharacter() -> String {
    var result = self
    let substr1 = String(self[startIndex]).uppercased()
    result.replaceSubrange(...startIndex, with: substr1)
    return result
  }
  
  var length : Int {
    return self.count
  }
  
  func words(with charset: CharacterSet = .alphanumerics) -> [String] {
    return self.unicodeScalars.split {
      !charset.contains($0)
      }.map(String.init)
  }
  
  func wrapped(after: Int = 70) -> String {
    var i = 0
    let lines = self.split(omittingEmptySubsequences: false) { character in
      switch character {
      case "\n",
           " " where i >= after:
        i = 0
        return true
      default:
        i += 1
        return false
      }
      }.map(String.init)
    return lines.joined(separator: "\n")
  }
  
  subscript (i: Int) -> Character {
    return self[self.index(self.startIndex, offsetBy: i)]
  }
  
  subscript (i: Int) -> String {
    return String(self[i] as Character)
  }
  
  subscript (r: Range<Int>) -> String {
    let start = self.index(self.startIndex, offsetBy: r.lowerBound)
    let end = self.index(self.startIndex, offsetBy: r.upperBound)
    
    return String(self[start...end])
  }
  
  var localized: String {
    return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
  }
  
  func loc(_ localizedKey:String) -> String {
    return NSLocalizedString(localizedKey, comment: "")
  }
  
  var urlEscaped : String {
    return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
  }
  
  var urlEscapedQuery : String {
    return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
  }
  
  func urlEncode() ->  String {
    let unreserved = "*-._~" // "!*'\"();:@&=+$,/?%#[]%- "
    var allowed = CharacterSet.alphanumerics
    allowed.insert(charactersIn: unreserved) // as per RFC 3986
    return self.addingPercentEncoding(withAllowedCharacters: allowed)!
  }
  
  
  var URLEncodedString: String {
    var allowedCharacterSet = NSMutableCharacterSet.urlQueryAllowed
    allowedCharacterSet.remove(charactersIn: "\n:#/?@!$&'()*+,;=") // these characters need to be left alone :p
    return self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)!
  }
  
  func left(numberOfCharacters: Int) -> String {
    guard numberOfCharacters <= self.length else { return self }
    let index = self.index(self.startIndex, offsetBy: numberOfCharacters)
    return String(self[..<index])
  }
  
  func right(numberOfCharacters: Int) -> String {
    guard numberOfCharacters <= self.length else { return self }
    let index = self.index(self.endIndex, offsetBy: -numberOfCharacters)
    return String(self[index...])
  }
  
  func trimmed() -> String {
    return self.trimmingCharacters(in: .whitespacesAndNewlines)
  }
  
  func removeWhitespaces() -> String {
    return self.replacingOccurrences(of: " ", with: "")
  }
  
  func replaceCommaWithDot() -> String {
    return self.replacingOccurrences(of: ",", with: ".")
  }
  
  //: ### Base64 --> UIImage
  func base64ToImage() -> UIImage {
    var img: UIImage = #imageLiteral(resourceName: "placeholder") //UIImage()
    if (!self.isEmpty) {
      let decodedData:NSData = NSData(base64Encoded: self, options:.ignoreUnknownCharacters)!
      let decodedimage:UIImage = UIImage(data: decodedData as Data)!
      img = decodedimage
    }
    return img
  }
  
  //: ### Base64 decoding a string
  func fromBase64() -> String? {
    guard let data = Data(base64Encoded: self) else {
      return nil
    }
    return String(data: data, encoding: .utf8)
  }
  
  //: ### Base64 encoding a string
  func toBase64() -> String {
    return Data(self.utf8).base64EncodedString()
  }
  
  func base64Encoded() -> String? {
    if let data = self.data(using: .utf8) {
      return data.base64EncodedString()
    }
    return nil
  }
  
  func base64Decoded() -> String? {
    if let data = Data(base64Encoded: self) {
      return String(data: data, encoding: .utf8)
    }
    return nil
  }
  
  func padding(length: Int) -> String {
    return self.padding(toLength: length, withPad: " ", startingAt: 0)
  }
  
  func padding(length: Int, paddingString: String) -> String {
    return self.padding(toLength: length, withPad: paddingString, startingAt: 0)
  }
  
  func leftPadding(toLength: Int, withPad character: Character) -> String {
    let newLength = self.count
    if newLength < toLength {
      return String(repeatElement(character, count: toLength - newLength)) + self
    } else {
      let index = self.index(self.startIndex, offsetBy: newLength - toLength)
      return String(self[index...])
    }
  }
  
  func isEmail() -> Bool {
    do {
      let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
      return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
    } catch {
      return false
    }
  }
  
  // String -> Date
  func toDate(withFormat format: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    guard let date = dateFormatter.date(from: self) else {
      dateFormatter.locale = Locale.current //Locale.init(identifier: "en-GB")
      return dateFormatter.date(from: self)!
    }
    return date
  }
  
  public func iso8601ToDate() -> Date {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
    return formatter.date(from: self)!
  }
  
  // String -> Double
  func toDouble() -> Double {
    guard let number = Double(self) else {
      let numberformatter = NumberFormatter()
      numberformatter.locale = Locale.current
      guard let numberFromNumberFormatter = numberformatter.number(from: self) else {
        return 0.00
      }
      return numberFromNumberFormatter.doubleValue
    }
    return number
  }
  
  // String -> Int
  func toInt() -> Int {
    guard let number = Int(self) else {
      let numberformatter = NumberFormatter()
      numberformatter.locale = Locale.current
      guard let numberFromNumberFormatter = numberformatter.number(from: self) else {
        return 0
      }
      return numberFromNumberFormatter.intValue
    }
    return number
  }
  
  // String -> Float
  func toFloat() -> Float {
    guard let number = Float(self) else {
      let numberformatter = NumberFormatter()
      numberformatter.locale = Locale.current
      guard let numberFromNumberFormatter = numberformatter.number(from: self) else {
        return 0
      }
      return numberFromNumberFormatter.floatValue
    }
    return number
  }
  
  // String -> UInt
  func toUInt() -> UInt {
    guard let number = UInt(self) else {
      let numberformatter = NumberFormatter()
      numberformatter.locale = Locale.current
      guard let numberFromNumberFormatter = numberformatter.number(from: self) else {
        return 0
      }
      return numberFromNumberFormatter.uintValue
    }
    return number
  }
  
  // String -> JSON String
  func toJSonString(data : Any) -> String {
    var jsonString = "";
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
      jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
    } catch {
      print(error.localizedDescription)
    }
    return jsonString;
  }
}
