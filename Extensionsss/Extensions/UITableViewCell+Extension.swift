//
//  UITableViewCell+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UITableViewCell {
  func showSeparator(){
    self.separatorInset = UIEdgeInsets(top: 0, left: self.frame.width, bottom: 0, right: 0);
  }
  
  func hideSeparator(){
    self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
  }
  
  func showSeparatorEndToEnd() {
    self.separatorInset = UIEdgeInsets.zero
  }
}
