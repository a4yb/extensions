//
//  UIColor+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor {
  static var officialApplePlaceholderGray: UIColor {
    return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
  }
}

public extension UIColor {
  
  // YESSS
  static let yesssBlue = UIColor(red: 0.0 / 255.0, green: 156.0 / 255.0, blue: 255.0 / 255.0, alpha: 1)
  static let yesssYellow = UIColor(red: 255.0 / 255.0, green: 228.0 / 255.0, blue: 0.0 / 255.0, alpha: 1)
  static let yesssGreen = UIColor(red: 138.0 / 255.0, green: 204.0 / 255.0, blue: 17.0 / 255.0, alpha: 1)
  
  // Metallic
  static let gold1 = UIColor(red: 218.0 / 255.0, green: 165.0 / 255.0, blue: 32.0 / 255.0, alpha: 1)
  static let gold2 = UIColor(red: 212.0 / 255.0, green: 175.0 / 255.0, blue: 55.0 / 255.0, alpha: 1)
  static let gold3 = UIColor(red: 212.0 / 255.0, green: 175.0 / 255.0, blue: 17.0 / 55.0, alpha: 1)
  static let silver = UIColor(red: 192.0 / 255.0, green: 192.0 / 255.0, blue: 192.0 / 255.0, alpha: 1)
  static let bronze = UIColor(red: 80.0 / 255.0, green: 50.0 / 255.0, blue: 20.0 / 255.0, alpha: 1)
  
  // Themes
  static let halloweenOrange = UIColor(red: 1.0 / 255.0, green: 165.0 / 255.0, blue: 26.0 / 255.0, alpha: 1)
  static let saintPatrickGreen = UIColor(red: 1.0 / 255.0, green: 165.0 / 255.0, blue: 26.0 / 255.0, alpha: 1)
  
  // Blue
  static let riderDarkBlueColor = UIColor(red: 50.0 / 255.0, green: 49.0 / 255.0, blue: 94.0 / 255.0, alpha: 1.0)         // #32315E
  static let riderBlueColor = UIColor(red: 0.0 / 255.0, green: 144.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)            // #0090FA
  
  // Gray
  static let riderDarkGrayColor = UIColor(red: 135.0 / 255.0, green: 152.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0)      // #8798AB
  static let riderGrayColor = UIColor(red: 170.0 / 255.0, green: 183.0 / 255.0, blue: 197.0 / 255.0, alpha: 1.0)          // #AAB7C5
  static let riderLightGrayColor = UIColor(red: 233.0 / 255.0, green: 238.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)     // #E9EEF5
  static let riderVeryLightGrayColor = UIColor(red: 246.0 / 255.0, green: 249.0 / 255.0, blue: 252.0 / 255.0, alpha: 1.0) // #F6F9FC
  
  // Green
  static let riderGreenColor = UIColor(red: 19.0 / 255.0, green: 181.0 / 255.0, blue: 125.0 / 255.0, alpha: 1.0)          // #13B57D
  static let riderLightGreenColor = UIColor(red: 173.0 / 255.0, green: 242.0 / 255.0, blue: 180.0 / 255.0, alpha: 1.0)    // #ADF2B4
  
  // Red
  static let darkRed = UIColor(red:0.25, green:0.25, blue:0.29, alpha:1.00)
  
  convenience init(colorWithHexValue value: Int, alpha:CGFloat = 1.0) {
    self.init(
      red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
      green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
      blue: CGFloat(value & 0x0000FF) / 255.0,
      alpha: alpha
    )
  }
  
  convenience init(hexString: String) {
    let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
    var int = UInt32()
    Scanner(string: hex).scanHexInt32(&int)
    let a, r, g, b: UInt32
    switch hex.count {
    case 3: // RGB (12-bit)
      (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
    case 6: // RGB (24-bit)
      (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
    case 8: // ARGB (32-bit)
      (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
    default:
      (a, r, g, b) = (1, 1, 1, 0)
    }
    self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
  }
  
}
