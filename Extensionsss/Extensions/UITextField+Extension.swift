//
//  UITextField+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

private var kAssociationKeyMaxLength: Int = 0

public extension UITextField {
  
  public func customPlaceHolder(text: String, color: UIColor) {
    self.attributedPlaceholder = NSAttributedString(string:text, attributes: [NSAttributedString.Key.foregroundColor: color])
  }
  
  @IBInspectable var maxLength: Int {
    get {
      if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
        return length
      } else {
        return Int.max
      }
    }
    set {
      objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
      addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
    }
  }
  
  @objc func checkMaxLength(textField: UITextField) {
    guard let prospectiveText = self.text,
      prospectiveText.count > maxLength
      else {
        return
    }
    
    let selection = selectedTextRange
    
    let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
    let substring = prospectiveText[..<indexEndOfText]
    text = String(substring)
    
    selectedTextRange = selection
  }
}
