//
//  Float+Extension.swift
//  stats
//
//  Created by Djamil Secco on 23/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

public extension Float {
  func rounded(decimalPlaces: Float) -> Float {
    guard decimalPlaces > -1 else { return self }
    let factor = pow(10.0, decimalPlaces)
    let valueToRound = self * factor
    let roundedValue = valueToRound.rounded(.awayFromZero)
    return roundedValue / factor
  }
  
  func string(fractionDigits:Int) -> String {
    let formatter = NumberFormatter()
    formatter.minimumFractionDigits = fractionDigits
    formatter.maximumFractionDigits = fractionDigits
    return formatter.string(from: self as NSNumber) ?? "\(self)"
  }
  
  func percentage(fractionDigits:Int) -> String {
    let formatter = NumberFormatter()
    formatter.minimumFractionDigits = fractionDigits
    formatter.maximumFractionDigits = fractionDigits
    let returnString = formatter.string(from: self as NSNumber) ?? "\(self)"
    return returnString  + " %"
  }
  
  func currencyFormat(fractionDigits:Int) -> String {
    let formatter = NumberFormatter()
    formatter.locale = Locale.current
    formatter.numberStyle = .currency
    formatter.minimumFractionDigits = fractionDigits
    formatter.maximumFractionDigits = fractionDigits
    return formatter.string(from: self as NSNumber) ?? "\(self)"
  }
}
