//
//  UIImage+Extension.swift
//  yessselec
//
//  Created by Djamil Secco on 23/08/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
  
  func toBase64String(compressionQuality: CGFloat) -> String {
    var imgString = String()
    if let imageData:Data = self.jpegData(compressionQuality: compressionQuality) {
      imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
    }
    return imgString
  }
  
  func template() -> UIImage {
    let image = self.withRenderingMode(.alwaysTemplate)
    return image
  }
  
  func fromLandscapeToPortrait(_ rotate: Bool!) -> UIImage {
    let container : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 320, height: 568))
    container.contentMode = UIView.ContentMode.scaleAspectFill
    container.clipsToBounds = true
    container.image = self
    
    UIGraphicsBeginImageContextWithOptions(container.bounds.size, true, 0);
    container.drawHierarchy(in: container.bounds, afterScreenUpdates: true)
    let normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if !rotate {
      return normalizedImage!
    } else {
      let rotatedImage = UIImage(cgImage: (normalizedImage?.cgImage!)!, scale: 1.0, orientation: UIImage.Orientation.left)
      
      UIGraphicsBeginImageContextWithOptions(rotatedImage.size, true, 1);
      rotatedImage.draw(in: CGRect(x: 0, y: 0, width: rotatedImage.size.width, height: rotatedImage.size.height))
      let normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
      UIGraphicsEndImageContext();
      
      return normalizedImage!
    }
  }
  
  static func imageWithColor(tintColor: UIColor) -> UIImage {
    let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
    tintColor.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
  }
  
  static func imageWithColor(tintColor: UIColor, width:Int, height:Int) -> UIImage {
    let rect = CGRect(x: 0, y: 0, width: width, height: height)
    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
    tintColor.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
  }
  
  func resizeImage(targetSize: CGSize) -> UIImage {
    let size = self.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
      newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
      newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    self.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
  }
  
  func scaledWithMaxWidthOrHeightValue(value: CGFloat) -> UIImage? {
    
    let width = self.size.width
    let height = self.size.height
    
    let ratio = width/height
    print(ratio)
    var newWidth = value
    var newHeight = value
    
    if ratio < 1 { // >
      newWidth = width * (newHeight/height)
    } else {
      newHeight = height * (newWidth/width)
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSize(width: newWidth, height: newHeight), false, 0)
    
    draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return image!
  }
  
  func scaled(withScale scale: CGFloat) -> UIImage? {
    
    let size = CGSize(width: self.size.width * scale, height: self.size.height * scale)
    
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    
    draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
    
    let image = UIGraphicsGetImageFromCurrentImageContext()
    
    UIGraphicsEndImageContext()
    
    return image
  }
}
